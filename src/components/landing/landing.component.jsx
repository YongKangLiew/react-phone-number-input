import React, {useState} from 'react';
import 'react-phone-number-input/style.css'
import PhoneInput, { formatPhoneNumber, formatPhoneNumberIntl, isValidPhoneNumber, isPossiblePhoneNumber } from 'react-phone-number-input/mobile'

export default function PhoneNumberComponent() {
  const [value, setValue] = useState()

  return (
    <>
      <div className="phone-input-content pt-100 form-group">
        <PhoneInput
          className="phone-input"
          international
          countryCallingCodeEditable={false}
          // countries={["AF","US","SE"]}
          // defaultCountry="AF"
          // addInternationalOption={false}
          placeholder="Enter phone number"
          value={value}
          onChange={setValue}
          error={value ? (isValidPhoneNumber(value) ? undefined : 'Invalid phone number') : 'Phone number required'}/>

        Is possible: {value && isPossiblePhoneNumber(value) ? 'true' : 'false'}
        <br/>
        Is valid: {value && isValidPhoneNumber(value) ? 'true' : 'false'}
        <br/>
        National: {value && formatPhoneNumber(value)}
        <br/>
        International: {value && formatPhoneNumberIntl(value)}
      </div>
    </>
  )

}