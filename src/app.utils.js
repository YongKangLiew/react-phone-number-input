let dataLayer;
if (typeof window !== 'undefined') {
  dataLayer = window.dataLayer || [];
}

export const numberWithCommas = (x) => {
  if (x) {
    return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
  }
}

export const pushPageViewEventToGA = () => {
  dataLayer.push({ event: 'pageView' });
}

export const handleAmount = (amount = "0") => {
  return parseFloat(amount).toFixed(2).replace(".00", "")
}