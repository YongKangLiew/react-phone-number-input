import React, {useEffect, useState} from "react";
import LandingComponent from '../components/landing/landing.component';
import Layout from '../components/shared/layout';

const IndexPage = () => {
  const [isMobile, setIsMobile] = useState(false)
  const [isTablet, setIsTablet] = useState(false)

  useEffect(() => {
    localStorage.clear();
  }, []);

  return (
    <div className="landing-page">
      <Layout >
        <LandingComponent />
      </Layout>
      {isMobile && <div className="pt-75"/>}
    </div>
  )
}

export default IndexPage;