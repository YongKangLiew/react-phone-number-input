require('dotenv').config({
  path: `.env.${process.env.NODE_ENV}`,
})

module.exports = {
  // pathPrefix: `/purchase`,
  plugins: [
    'gatsby-plugin-react-helmet',
    {
      resolve: `gatsby-plugin-create-client-paths`,
      options: { prefixes: [`//*`] },
    },
    `gatsby-plugin-sass`
  ]
}