## C2P Onboarding Portal
[Gatsby] https://www.gatsbyjs.com/get-started/ is an open-source framework that combines functionality from React, GraphQL and Webpack into a single tool for building static websites and apps


## Installation and Setup Instructions
Clone down this repository. You will need node and npm installed globally on your machine.

## Production Branch:
Master

## Production URL:
//

## Staging URL:
//

## Installation:

```bash
$ npm install
```

## Run project locally:

```bash
$ gatsby develop
```

## Build project:

```bash
$ gatsby build
```

